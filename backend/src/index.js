const express = require('express');
const cors = require('cors');
const routes = require('./routes/routes');
const ip = require('ip');

const app = express();

let ipServidor = ip.address();

app.use(cors());
app.use(express.json());

app.use(routes);

app.listen(3333, ipServidor, () => console.log(`Servidor funcionando em ${ipServidor}`));

/**
 * ROTAS / RECURSOS
 */

/**
 * metodos HTTP:
 *
 * GET: buscar uma informação
 * POST: criar uma informação
 * PUT: alterar uma informação
 * DELETE: deletar uma informação
 */

/**
 * query params: parametros nomeados enviados na rota apos "?" filtros, paginacao
 * route params: identificar recursos
 * request body: corpo da requisicao, utilizado para criar ou alterar recursos
 */
