const express = require('express');

const OngController = require('../controllers/Ong/OngController');
const IncidentController = require('../controllers/Incident/IncidentController');
const ProfileController = require('../controllers/Profile/ProfileController');
const SessionController = require('../controllers/Session/SessionController');

const routes = express.Router();

routes.post('/sessions', SessionController.create)

routes.get('/ongs', OngController.index);
routes.post('/ongs', OngController.create);

routes.get('/profile', ProfileController.index)

routes.get('/incidents', IncidentController.index);
routes.post('/incidents', IncidentController.create);
routes.delete('/incidents/:id', IncidentController.delete);

module.exports = routes;